from flask import Flask
from flask import request

app = Flask(__name__, static_url_path='/app/static')
app.config['SECRET_KEY'] = 'very_secret_key'

from app import routes