
from wtforms import TextAreaField, SubmitField
from flask_wtf import FlaskForm


class RecipeInputForm(FlaskForm):
    recipe = TextAreaField('Recipe')
    submit = SubmitField('Send')
