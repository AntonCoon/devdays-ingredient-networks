# -*- coding: utf-8 -*-

from app import app
from flask import render_template
from app.forms import RecipeInputForm
from flask import request
from sklearn.externals import joblib
import sys
import os

sys.path.append("..")
from core import read_all_recipes, make_graph, subgraph_and_suggest, draw_graph
from rating import train_basic_model, add_ingr_pred, suggest_top_n_ingr


# initialisation
dataset_dir = "../../filtered_dataset_with_white_list"
rating_file = '../data/models/rating.pkl'
difficulty_file = '../data/models/difficulty.pkl'
ingr_add_suggest_file = '../data/models/ingr_add_suggest.pkl'
difficulty_levels = {0: "легко", 1: "средне", 2: "сложно"}
main_graph = make_graph('../graph_new.txt')


def load_models(force_new=False):
    if os.path.exists(rating_file) and not force_new:
        rat = joblib.load(rating_file)
    else:
        rat = train_basic_model(all_recipes, 0)
    if os.path.exists(difficulty_file) and not force_new:
        diff = joblib.load(difficulty_file)
    else:
        diff = train_basic_model(all_recipes, 2)
    if os.path.exists(ingr_add_suggest_file) and not force_new:
        add_suggest = joblib.load(ingr_add_suggest_file)
    else:
        add_suggest = add_ingr_pred(all_recipes)
    return rat, diff, add_suggest


def save_models(rat, diff, add_suggest):
    if not os.path.exists(rating_file):
        joblib.dump(rat, rating_file, compress=9)
    if not os.path.exists(difficulty_file):
        joblib.dump(diff, difficulty_file, compress=9)
    if not os.path.exists(ingr_add_suggest_file):
        joblib.dump(add_suggest, ingr_add_suggest_file, compress=9)


all_recipes = read_all_recipes(dataset_dir)
rating, difficulty, ingr_add_suggest = load_models(force_new=False)
save_models(rating, difficulty, ingr_add_suggest)


def get_basic_prediction(model, recipe):
    vec = model[1].transform([recipe]).toarray()
    return model[0].predict(vec)[0]


def get_predictions(recipe):
    subgrph, graph_suggested = subgraph_and_suggest(main_graph, recipe.strip().split())
    answer = {
        "rating": get_basic_prediction(rating, recipe),
        "difficulty": difficulty_levels[get_basic_prediction(difficulty, recipe)],
        "add_ings": " ".join(suggest_top_n_ingr(
            ingr_add_suggest[0],
            ingr_add_suggest[1],
            ingr_add_suggest[2],
            recipe, 3)),
        "add_ings_graph": " ".join(graph_suggested),
        "path_to_recipe_network": draw_graph(subgrph, recipe.strip().split(), './app/static/net_rec.jpg')
    }

    return answer


@app.route('/', methods=['GET', 'POST'])
def index():
    form = RecipeInputForm()
    if request.method == 'POST':
        recipe_text = request.form["recipe"]
        ans = get_predictions(recipe_text)
        return render_template(
            "index.html",
            not_empty=True,
            rating="{:3.2f}".format(ans["rating"]),
            difficulty=ans["difficulty"],
            add_ings=ans["add_ings"], add_ings_graph=ans["add_ings_graph"],
            remove_ingrid=None,
            form=form)
    else:
        print("request method:", request.method)
        return render_template("index.html", not_empty=False, form=form)
