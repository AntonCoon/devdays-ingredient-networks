from sklearn.model_selection import train_test_split
from catboost import CatBoostRegressor, CatBoostClassifier
from sklearn.svm import SVR
from sklearn.linear_model import SGDClassifier

from core import *


# basic


def make_data(recipes, target_param):
    cond, target = target_param
    recs = list(filter(cond, recipes))
    print("Object count:", len(recs))
    X, vectorizer = recipes_vec(recs)
    y = np.zeros(len(recs))
    for i in range(len(recs)):
        y[i] = target(recs[i])
    X_tr, X_te, y_tr, y_te = train_test_split(X, y, train_size=0.7, random_state=997)
    return X_tr, y_tr, X_te, y_te, vectorizer


def predict_smth_svm(X_tr, y_tr, X_te, y_te, reg=True):
    if reg:
        model = SVR(gamma=0.001, C=1.0, epsilon=0.2, max_iter=100)
    else:
        model = SGDClassifier(loss='hinge', penalty='l2', alpha=0.0001, max_iter=100)
    model.fit(X_tr, y_tr)
    if reg:
        print(np.mean(np.square(model.predict(X_te) - y_te)))
    else:
        print(np.mean(model.predict(X_te) == y_te))
    return model


def predict_smth_cb(X_tr, y_tr, X_te, y_te, reg=True):
    if reg:
        model = CatBoostRegressor(iterations=100, thread_count=4)
    else:
        model = CatBoostClassifier(iterations=100, thread_count=4, custom_loss=['Accuracy'])
    model.fit(X_tr, y_tr, eval_set=(X_te, y_te), plot=True, silent=True)
    return model


def train_basic_model(recipes, ind):
    difficults = {"easy": 0, "intermediate": 1, "advanced": 2}
    target_params = [
        (True, (lambda x: "rating" in x and "value" in x["rating"], lambda rec: float(rec["rating"]["value"]))),
        (True, (lambda x: "rating" in x and "count" in x["rating"], lambda rec: float(rec["rating"]["count"]))),
        (False, (lambda rec: "difficulty" in rec and rec["difficulty"] in difficults,
                 lambda rec: difficults[rec["difficulty"]])),
        (True, (lambda rec: "difficulty" in rec and rec["difficulty"] in difficults,
                lambda rec: difficults[rec["difficulty"]])),
        (True, (lambda rec: "time" in rec, lambda rec: parse_time(rec["time"]))),
    ]
    X_tr, y_tr, X_te, y_te, vectorizer = make_data(recipes, target_params[ind][1])
    model = predict_smth_cb(X_tr, y_tr, X_te, y_te, reg=target_params[ind][0])
    return model, vectorizer


# ingredient suggest


def reverse_map(vect):
    map = {}
    for k in vect.vocabulary_.keys():
        map[vect.vocabulary_[k]] = k
    return map


def make_addition_dataset(recipes):
    _, vectorizer = recipes_vec(recipes)
    X, y = [], []
    for i in range(min(1000, len(recipes))):
        ingrids = recipes[i]["ingredients"]
        for j in range(len(ingrids)):
            X.append(" ".join([ingr for ingr in ingrids if ingr != ingrids[j]]))
            y.append(ingrids[j])
    X = vectorizer.transform(X).toarray()
    y = np.array([np.argmax(v) for v in vectorizer.transform(y).toarray()])
    return X, y, vectorizer


def add_ingr_pred(recipes):
    X, y, vectorizer = make_addition_dataset(recipes)
    model = SGDClassifier(loss='log', penalty='l2', alpha=0.0001, max_iter=100)
    model.fit(X, y)
    return model, vectorizer, reverse_map(vectorizer)


def suggest_top_n_ingr(model, vectorizer, reverse_voc, recipe, n):
    if n <= 0:
        return []
    vec = vectorizer.transform([recipe]).toarray()
    sugg = reverse_voc[model.predict(vec)[0]]
    if sugg in recipe.split('\n') or sugg in recipe.split(' '):
        return []
    return [sugg] + suggest_top_n_ingr(model, vectorizer, reverse_voc, recipe + " " + sugg, n - 1)
