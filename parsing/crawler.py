import requests
import json
import os
import re
import sys
import argparse


def dataset_creation(folder_name="dataset", recipe_list="recipe_list", start=0, finish=None):
    i = 0
    fails1 = 0
    fails2 = 0
    fails3 = 0
    fails4 = 0
    fails5 = 0
    if not os.path.exists(folder_name):
        os.makedirs(folder_name)
    with open(recipe_list, "r") as f:
        for recipe in f:
            if i < start:
                i += 1
                continue
            if finish is not None and i > finish:
                break
            data = {}
            page_text = requests.get(recipe).text
            reg = re.search('<script type=\"application/ld\+json\">([\s\S]*?)</script>', page_text)
            if reg:
                j = json.loads(reg.group(1))
                if "name" in j:
                    data["name"] = j["name"]
                if "recipeIngredient" in j:
                    data["ingredients"] = j["recipeIngredient"]
                data["rating"] = {}
                if "aggregateRating" in j:
                    if "ratingValue" in j["aggregateRating"]:
                        data["rating"]["value"] = j["aggregateRating"]["ratingValue"]
                    if "reviewCount" in j["aggregateRating"]:
                        data["rating"]["count"] = j["aggregateRating"]["reviewCount"]
                if "recipeInstructions" in j:
                    data["instruction"] = j["recipeInstructions"]
                if "cookTime" in j:
                    data["time"] = j["cookTime"]
            else:
                fails1 += 1

            reg = re.search('mdManager\.addParameter\(\"TargetedTerms\"\, \"([\s\S]*?)\"\)', page_text)
            if reg:
                data["tags"] = reg.group(1).split(";")
            else:
                fails2 += 1
                data["tags"] = []

            reg = re.search('mdManager\.addParameter\(\"Difficulty\"\, \"([\s\S]*?)\"\)', page_text)
            if reg:
                data["difficulty"] = reg.group(1)
            else:
                fails3 += 1
                data["difficulty"] = ""

            reg = re.search('mdManager\.addParameter\(\"Cuisine\"\, \"([\s\S]*?)\"\)', page_text)
            if reg:
                data["cuisine"] = reg.group(1)
            else:
                fails4 += 1
                data["cuisine"] = []

            reg = re.search('mdManager\.addParameter\(\"Technique\"\, \"([\s\S]*?)\"\)', page_text)
            if reg:
                data["technique"] = reg.group(1)
            else:
                fails5 += 1
                data["technique"] = []
            out = open(folder_name + "/" + str(i) + ".json", "w")
            json.dump(data, out)
            i += 1

            if i % 100:
                sys.stdout.write('\r')
                sys.stdout.write(
                    str(i) + " " + str(fails1) + " " + str(fails2) + " " + str(fails3) + " " + str(fails4) + " " + str(
                        fails5))
                sys.stdout.flush()


def arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dir', required=True)
    parser.add_argument('-l', '--list', required=True)
    parser.add_argument('-f', '--from_ind', default=0, type=int)
    parser.add_argument('-t', '--to', default=None, type=int)
    return parser


def main():
    args = arg_parser().parse_args()
    dataset_creation(args.dir, args.list, args.from_ind, args.to)


main()
