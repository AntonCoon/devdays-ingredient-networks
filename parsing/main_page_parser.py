import bs4
import requests


full_numbers_of_recipe = 7060
with open("all_recipe.txt", "w") as result:
    for index in range(1, full_numbers_of_recipe):
        html = requests.get("https://www.foodnetwork.com/search/p/" + str(index) + "/CUSTOM_FACET:RECIPE_FACET").text
        soup = bs4.BeautifulSoup(html, "lxml")
        productDivs = soup.find_all('h3', attrs={'class': 'm-MediaBlock__a-Headline'})
        for h3 in productDivs:
            result.write("https:" + h3.find('a')['href'] + "\n")
        if index % 100 == 0:
            print(index / full_numbers_of_recipe, "%")
