import os
import json
import re
from sklearn.feature_extraction.text import CountVectorizer
import networkx as nx
import matplotlib.pyplot as plt


def normalize(s):
    return re.sub(" +", " ", re.sub("[^ \w]|\d", " ", s.lower()))


def filter_ingrids(ingrids):
    words = []
    for s in ingrids:
        words += list(filter(lambda x: x, normalize(s).split(' ')))
    return words


def read_recipe(filename):
    with open(filename, 'r') as file:
        rec = json.loads(file.read())
        if "ingredients" not in rec:
            raise ValueError
        rec["ingredients"] = filter_ingrids(rec["ingredients"])
        return rec


def read_all_recipes(dir):
    all_rec = []
    for file in os.listdir(dir):
        try:
            all_rec.append(read_recipe(dir + "/" + file))
        except ValueError:
            pass
    return all_rec


def recipes_vec(recipes):
    vectorizer = CountVectorizer()
    vec = vectorizer.fit_transform([" ".join(rec["ingredients"]) for rec in recipes]).toarray()
    return vec, vectorizer


def parse_time(s):
    times = re.split('[PYMDTHS]', s)
    return int(times[5]) * 60 + int(times[6])


# graphs


def make_graph(file):
    G = nx.Graph()
    f = open(file)
    for row in f:
        line = row.split()
        weight_ = float(line[2])
        G.add_edge(line[0], line[1], weight=weight_)
    return G


def draw_graph(
        G, color_node, file_path,
        strange=False,
        node_init_size=10,
        node_scale=10,
        node_groups=10,
        edge_init_size=0.6,
        edge_scale=0.2,
        edge_init_transp=0.09,
        edge_groups=10):

    if edge_init_transp < 0 or edge_init_transp > 1:
        edge_init_transp = 0

    m = list(G.node)
    e = list(G.edges(data=True))

    count = [0] * len(m)
    for i in range(len(m)):
        for j in range(len(e)):
            if m[i] in e[j]:
                count[i] += e[j][2]['weight']

    max_node = max(count)
    max_edge = max([i[2]['weight'] for i in e])
    k = [(m[i], {'weight': count[i]}) for i in range(len(m))]

    r = []
    step = max_edge/edge_groups
    i = max_edge/edge_groups
    if not strange:
        pos = nx.spring_layout(G, scale=30, k=13)
    else:
        pos = nx.kamada_kawai_layout(G)
    while i <= max_edge:
        large = [(u, v) for (u, v, d) in G.edges(data=True) if (i - step) < d['weight'] <= i]
        r = r + [large]
        i = i + step

    for l in range(len(r)):
        nx.draw_networkx_edges(G, pos,
                               edgelist=r[l],
                               width=edge_scale*l + edge_init_size,
                               color='b', edge_color='black',
                               alpha=(1 - edge_init_transp) * l/edge_groups + edge_init_transp)

    node = []
    color_map = []
    step = max_node / node_groups
    i = max_node / node_groups
    while i <= max_node:
        tcolor = []
        nodelarge = [u for (u, d) in k if (i - step) < d['weight'] <= i]
        for one in nodelarge:
            if one in color_node:
                tcolor.append('yellow')
            else:
                tcolor.append('red')
        node = node + [nodelarge]
        color_map = color_map + [tcolor]
        i = i + step

    for l in range(len(node)):
        nx.draw_networkx_nodes(G, pos,
                               nodelist=node[l],
                               node_size=node_scale * l + node_init_size,
                               node_color=color_map[l])

    nx.draw_networkx_labels(G, pos, font_size=5, font_family='sans-serif')
    plt.axis('off')
    # hardcode aware!
    if os.path.exists(file_path):
        os.remove(file_path)
    plt.savefig(file_path)

    return file_path


def subgraph_and_suggest(G, node_list, nbr=10):
    suggested_number = int(len(node_list) * 0.5) + 1
    candidates = {}
    edge_info = {}
    sG = nx.Graph()
    for elem in list(G.edges(data=True)):
        if (elem[1] in node_list) and (elem[0] in node_list):
            sG.add_edge(elem[0], elem[1], weight=elem[2]['weight'])
        if (elem[1] in node_list) or (elem[0] in node_list):
            for i in [0, 1]:
                if not (elem[i] in node_list):
                    if elem[i] in candidates:
                        candidates[elem[i]] += elem[2]['weight']
                    else:
                        candidates[elem[i]] = elem[2]['weight']
            edge_info[(elem[0], elem[1])] = elem[2]['weight']
    sorted_cand = sorted(candidates.items(), key=lambda x: x[1], reverse=True)
    if len(sorted_cand) > nbr:
        sorted_cand = sorted_cand[:nbr]

    for new in sorted_cand:
        for old in node_list:
            if (new[0], old) in edge_info:
                sG.add_edge(new[0], old, weight=edge_info[(new[0], old)])
            if (old, new[0]) in edge_info:
                sG.add_edge(old, new[0], weight=edge_info[(old, new[0])])
    return sG.copy(), [ingrid[0] for ingrid in sorted_cand[:min(len(candidates), suggested_number)]]


# recipe = ["basil", "tomatoes", "onion", "garlic", "olive", "oil", "pepper", "flank", "salt", "clove", "flour",
#           "parsley", "water"]
# full_graph = make_graph('graph_new.txt')
# sub, suggest = subgraph_and_suggest(full_graph, recipe)
# draw_graph(full_graph, [], './graph_plot.jpg')
# print(recipe)
# print(suggest)


